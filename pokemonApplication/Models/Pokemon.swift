//
//  Pokemon.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

class Pokemon: Codable {
    enum CodingKeys: String, CodingKey {
        case url
        case name
    }
    
    private(set) var url: String
    private(set) var name: String
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.url = try container.decode(String.self, forKey: .url)
    }
}
