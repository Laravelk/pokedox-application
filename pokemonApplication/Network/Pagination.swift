//
//  Pagination.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

enum Pagination {
    case first
    case last
    case number(Int)
}
