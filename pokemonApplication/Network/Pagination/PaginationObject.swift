//
//  PaginationObject.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 04.10.2021.
//

import Foundation

enum PaginationObjectState<T> {
    case initial(pageLimit: Int)
    case continuing(PaginationObject<T>, PaginationOperationState)
}

enum PaginationOperationState {
    case first
    case last
    case next
    case previous
    case page(Int)
}

final class PaginationObject<T>: Codable {
    enum CodingKeys: String, CodingKey {
        case count
        case next
        case previous
        case results
    }
    
    private(set) var count: Int?
    private(set) var next: String?
    private(set) var previous: String?
    private(set) var results: [Pokemon]?
    
    // current url for pagination page
    private(set) var current: String = ""
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.count = try container.decodeIfPresent(Int.self, forKey: .count)
        self.next = try container.decodeIfPresent(String.self, forKey: .next)
        self.previous = try container.decodeIfPresent(String.self, forKey: .previous)
        results = try container.decodeIfPresent([Pokemon].self, forKey: .results)
    }
    
    private(set) var limit: Int = 0
    private(set) var offset: Int = 0
    
    var pages: Int {
        guard let count = count else { return 0 }
        return Int((Float(count) / Float(limit)).rounded(.up))
    }

    // MARK: - PaginationObject operations
    
    func update<T>(with objectState: PaginationObjectState<T>, currentUrl: String) {
        switch objectState {
        case .initial(let pageLimit):
            offset = 0
            limit = pageLimit
        case .continuing(let pagedObject, let pagedOperation):
            limit = pagedObject.limit
            offset = getOffset(pagedOperation)
        }
        
        self.current = currentUrl
    }
    
    func getOffset(_ pagedOperation: PaginationOperationState) -> Int {
        switch pagedOperation {
        case .first:
            return 0
        case .last:
            guard let count = count else { return 0 }
            let remainingCount = count % limit
            return count - remainingCount
        case .next:
            return min(offset + limit, getOffset(.last))
        case .previous:
            return max(offset - limit, 0)
        case .page(let page):
            guard page > 0 else { return 0 }
            guard page < pages else { return getOffset(.last) }
            
            return limit * page
        }
    }
}
