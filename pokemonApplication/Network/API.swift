//
//  API.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

protocol APIType {
    var path: String { get }
}

extension APIType {
    func createUrl(baseUrl: String) -> URL? {
        URL(string: baseUrl + path)
    }
    
    func createUrl<T>(baseUrl: String, paginationState: PaginationObjectState<T>) -> URL? {
        guard var url = createUrl(baseUrl: baseUrl) else { return nil }
        
        var pageLimit = 0
        var offset = 0
        
        switch paginationState {
        case .initial(let limit):
            pageLimit = limit
        case .continuing(let paginationObject, let paginationOperation):
            pageLimit = paginationObject.limit
            offset = paginationObject.getOffset(paginationOperation)
        }
        
        url.appendQuery(parameters: [
            "limit": String(pageLimit),
            "offset": String(offset)
        ])
        
        return url
    }
    
    func createUrlRequest(
        baseUrl: String,
        method: HTTPMethod,
        headers: [HTTPHeader]?,
        httpBody: Data? = nil
    ) throws -> URLRequest {
        guard let url = createUrl(baseUrl: baseUrl) else { throw HTTPError.invalidRequest }
        return createUrlRequest(url: url, method: method, headers: headers)
    }
    
    func createUrlRequest<T>(
        baseUrl: String,
        paginationState: PaginationObjectState<T>,
        method: HTTPMethod,
        headers: [HTTPHeader]?,
        httpBody: Data? = nil
    ) throws -> URLRequest {
        guard let url = createUrl(baseUrl: baseUrl, paginationState: paginationState) else { throw HTTPError.invalidRequest }
        return createUrlRequest(
            url: url,
            method: method,
            headers: headers,
            httpBody: httpBody
        )
    }
}

private extension APIType {
    func createUrlRequest(
        url: URL,
        method: HTTPMethod,
        headers: [HTTPHeader]?,
        httpBody: Data? = nil
    ) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        if let headers = headers {
            request.appendHeaders(headers)
        }
        request.httpBody = httpBody
        return request
    }
}
