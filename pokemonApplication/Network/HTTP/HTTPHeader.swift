//
//  HTTPHeader.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

enum HTTPHeader {
    case contentType(MediaType)
    case accept(MediaType)
    
    var key: String {
        switch self {
        case .contentType:
            return "Content-Type"
        case .accept:
            return "Accept"
        }
    }
    
    var value: String {
        switch self {
        case .contentType(let mediaType):
            return mediaType.headerValue
        case .accept(let mediaType):
            return mediaType.headerValue
        }
    }
}

enum MediaType {
    case json
    case html
    case plainText
    case png
    case jpeg
    
    var headerValue: String {
        switch self {
        case .json:
            return "application/json"
        case .html:
            return "text/html"
        case .plainText:
            return "text/plain"
        case .png:
            return "image/png"
        case .jpeg:
            return "image/jpeg"
        }
    }
}
