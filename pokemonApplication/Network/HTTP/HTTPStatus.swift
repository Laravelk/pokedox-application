//
//  HTTPStatus.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

enum HTTPStatus {
    case success
    case failure
    
    init(statusCode: Int) {
        if statusCode >= 200 && statusCode <= 300 {
            self = .success
        } else {
            self = .failure
        }
    }
}
