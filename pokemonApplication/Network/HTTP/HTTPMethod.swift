//
//  HTTPMethod.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

enum HTTPMethod: String {
    case get, post, put, delete, patch
}
