//
//  HTTPError.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

enum HTTPError: Error {
    case serverError
    case invalidRequest
    case timeout
    case forbidden
    case noNetwork
    case jsonParsingError
    case other(Error)
    
    // MARK: Description
    
    var description: String {
        switch self {
        case .serverError:
            return NSLocalizedString("Server error", comment: "Server error")
        case .invalidRequest:
            return NSLocalizedString("Invalid Request", comment: "Invalid Request")
        case .timeout:
            return NSLocalizedString("Timeout", comment: "Time out")
        case .forbidden:
            return NSLocalizedString("Forbidden", comment: "Forbidden")
        case .noNetwork:
            return NSLocalizedString("No network", comment: "No network")
        case .jsonParsingError:
            return NSLocalizedString("Json parsing error", comment: "Json parsing error")
        case .other(let code):
            return NSLocalizedString(code.localizedDescription, comment: code.localizedDescription)
        }
    }
    
    // MARK: Code
    
    var code: Int {
        switch self {
        case .serverError:
            return 501
        case .invalidRequest:
            return 401
        case .timeout:
            return -1001
        case .forbidden:
            return 403
        case .noNetwork:
            return -1009
        case .other(let error as NSError):
            return error.code
        default:
            return 499
        }
    }
}
