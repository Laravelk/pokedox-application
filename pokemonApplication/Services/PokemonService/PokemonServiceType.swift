//
//  PokemonServiceType.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

protocol PokemonServiceType: AnyObject {
    func getPokemons(completion: @escaping (_ result: Result<[Pokemon], Error>) -> Void)
    func getPokemons<T>(paginationObjectState: PaginationObjectState<T>, completion: @escaping (_ result: Result<PaginationObject<T>, Error>) -> Void) where T: Pokemon
}
