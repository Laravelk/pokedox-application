//
//  PokemonService.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

final class PokemonService: PokemonServiceType {
    private let httpWebService: HTTPWebServiceType
    
    init(httpWebService: HTTPWebServiceType) {
        self.httpWebService = httpWebService
    }
    
    func getPokemons(completion: @escaping (Result<[Pokemon], Error>) -> Void) {
        httpWebService.request(endpoint: API.getPokemon, method: .get, headers: nil, httpBody: nil) { result in
            result.decode(completion: completion)
        }
    }
    
    func getPokemons<T>(paginationObjectState: PaginationObjectState<T> = .initial(pageLimit: 40), completion: @escaping (Result<PaginationObject<T>, Error>) -> Void) where T: Pokemon {
        httpWebService.requestWithPaginated(
            endpoint: API.getPokemon,
            paginationState: paginationObjectState,
            method: .get,
            headers: nil,
            httpBody: nil,
            completion: completion
        )
    }
}

// MARK: - API

extension PokemonService {
    enum API: APIType {
        case getPokemon
        
        var path: String {
            switch self {
            case .getPokemon:
                return "/pokemon"
            }
        }
    }
}
