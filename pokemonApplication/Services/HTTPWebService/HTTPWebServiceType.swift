//
//  HTTPWebServiceType.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

protocol HTTPWebServiceType: AnyObject {
    var session: URLSession { get }
    var baseUrl: String { get }
    
    func request(
        endpoint: APIType,
        method: HTTPMethod,
        headers: [HTTPHeader]?,
        httpBody: Data?,
        completion: @escaping (_ result: Result<Data, Error>) -> Void)
    
    func requestWithPaginated<T>(
        endpoint: APIType,
        paginationState: PaginationObjectState<T>,
        method: HTTPMethod,
        headers: [HTTPHeader]?,
        httpBody: Data?,
        completion: @escaping (_ result: Result<PaginationObject<T>, Error>) -> Void
    )
}
