//
//  HTTPWebService.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

final class HTTPWebService {
    var session: URLSession
    var baseUrl: String
    
    init(session: URLSession, baseUrl: String) {
        self.session = session
        self.baseUrl = baseUrl
    }
}

// MARK: - HTTPWebServiceType

extension HTTPWebService: HTTPWebServiceType {
    func request(
        endpoint: APIType,
        method: HTTPMethod = .get,
        headers: [HTTPHeader]? = nil,
        httpBody: Data? = nil,
        completion: @escaping (_ result: Result<Data, Error>) -> Void
    ) {
        do {
            let request = try endpoint.createUrlRequest(baseUrl: baseUrl, method: method, headers: headers, httpBody: httpBody)
            
            session.request(request) { result in
                completion(result)
            }
        }
        catch let error {
            completion(.failure(error))
        }
    }
    
    func requestWithPaginated<T>(
        endpoint: APIType,
        paginationState: PaginationObjectState<T>,
        method: HTTPMethod = .get,
        headers: [HTTPHeader]? =  nil,
        httpBody: Data? = nil,
        completion: @escaping (_ result: Result<PaginationObject<T>, Error>) -> Void
    ) {
        do {
            let request = try endpoint.createUrlRequest(
                baseUrl: baseUrl,
                paginationState: paginationState,
                method: method,
                headers: headers,
                httpBody: httpBody
            )
            
            guard let url = request.url else {
                completion(.failure(HTTPError.invalidRequest))
                return
            }
            
            session.request(request) { result in
                switch result {
                case .success(let data):
                    do {
                        let pagedObject = try PaginationObject<T>.decode(from: data)
                        pagedObject.update(with: paginationState, currentUrl: url.absoluteString)
                        completion(.success(pagedObject))
                    } catch let error {
                        completion(.failure(HTTPError.other(error)))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
        } catch let error {
            completion(.failure(HTTPError.other(error)))
        }
    }
}
