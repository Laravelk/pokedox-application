//
//  CommonContext.swift
//  pokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

final class CommonContext {
    let httpWebService: HTTPWebServiceType
    let pokemonService: PokemonServiceType
    
    private let urlSession = URLSession(configuration: .default)
    private let baseUrl = "https://pokeapi.co/api/v2"
    
    init() {
        httpWebService = HTTPWebService(session: urlSession, baseUrl: baseUrl)
        pokemonService = PokemonService(httpWebService: httpWebService)
    }
}

protocol HasHttpWebService { var httpWebService: HTTPWebServiceType { get } }
extension CommonContext: HasHttpWebService {}

protocol HasPokemonService { var pokemonService: PokemonServiceType { get } }
extension CommonContext: HasPokemonService {}
