//
//  LaunchCoordinator.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation
import UIKit

final class LaunchCoordinator {
    typealias Context = HasHttpWebService & PokedexCoordinator.Context
    
    private let context: Context
    
    private weak var navigationController: UINavigationController?
    private weak var windowNavigation: SingleNavigationControllerType?
    
    init(context: Context, windowNavigation: SingleNavigationControllerType) {
        self.context = context
        self.windowNavigation = windowNavigation
        startController()
    }
    
    func startController() {
        let nc = prepareNavigationController()
        let pokedexCoordinator = PokedexCoordinator(context: context, navigation: nc)
        let controller = pokedexCoordinator.makeInitial()
        nc.viewControllers = [controller]
        windowNavigation?.put(nc)
    }
    
    private func prepareNavigationController() -> UINavigationController {
        let nc = UINavigationController()
        nc.setNavigationBarHidden(true, animated: false)
        
        self.navigationController = nc
        return nc
    }
}
