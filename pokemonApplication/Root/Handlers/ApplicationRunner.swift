//
//  ApplicationRunner.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import UIKit

final class ApplicationRunner {
    private let context: CommonContext
    private let window: UIWindow
    
    init(commonContext: CommonContext, window: UIWindow) {
        self.context = commonContext
        self.window = window
    }
}

// MARK: - AppDelegateHandlerType

extension ApplicationRunner: SceneDelegateHandlerType {
    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) { run() }
    
    func application(
        _ application: UIApplication,
        willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
    ) -> Bool {
        run()
        return true
    }
}

// MARK: - Run function

private extension ApplicationRunner {
    func run() {
        _ = LaunchCoordinator(context: context, windowNavigation: window)
    }
}
