//
//  SceneDelegate.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    private(set) var applicationRunner: ApplicationRunner!
    private var handlers: [SceneDelegateHandlerType] = []

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        run(with: scene)
        handlers.forEach { $0.scene(scene, willConnectTo: session, options: connectionOptions) }
        window?.makeKeyAndVisible()
    }
}

// MARK: - AppDelegate runner

private extension SceneDelegate {
    func run(with scene: UIScene) {
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            self.window = window
        
            let applicationRunner = ApplicationRunner(commonContext: CommonContext(), window: window)
            self.handlers = [applicationRunner]
        }
    }
}
