//
//  PokedexCoordinator.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import UIKit

final class PokedexCoordinator: CoordinatorType {
    typealias Context = PokedexViewModel.Context
    
    private let context: Context
    private weak var navigation: UINavigationController?
    
    init(context: Context, navigation: UINavigationController) {
        self.context = context
        self.navigation = navigation
    }
    
    func makeInitial() -> UIViewController {
        let controller = PokedexViewController.loadFromStoryboard()
        let viewModel = PokedexViewModel(context: context)
        
        controller.viewModel = viewModel
        navigation?.pushViewController(controller, animated: true)
        return controller
    }
}
