//
//  PokedexViewController.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import UIKit

protocol PokedexViewControllerType: AnyObject {
    
}

final class PokedexViewController: UIViewController, BaseViewController {
    typealias ViewModel = PokedexViewModelType
    var viewModel: ViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel?.viewController = self
        view.backgroundColor = .brown
    }
}

// MARK: - PokedexViewControllerType

extension PokedexViewController: PokedexViewControllerType {
    
}
