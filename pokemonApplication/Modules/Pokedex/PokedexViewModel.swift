//
//  PokedexViewModel.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

protocol PokedexViewModelType: AnyObject {
    var viewController: PokedexViewControllerType? { get set }
}

final class PokedexViewModel {
    typealias Context = HasHttpWebService & HasPokemonService
    
    weak var viewController: PokedexViewControllerType?
    
    private let context: Context
    
    init(context: Context) {
        self.context = context
        loadPokemons()
    }
}

// MARK: - PokedexViewModelType

extension PokedexViewModel: PokedexViewModelType {
    
}

// MARK: - Loader

private extension PokedexViewModel {
    func loadPokemons() {
        context.pokemonService.getPokemons(paginationObjectState: .initial(pageLimit: 40)) { result in
            switch result {
            case .success(let object):
                print(object.results?.count)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        context.pokemonService.getPokemons { result in
            switch result {
            case .success(let pok):
                print(pok)
            case .failure(let error):
                break
            }
        }
    }
}
