//
//  URLSession+Extension.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

extension URLSession {
    func request(_ request: URLRequest, completion: @escaping (_ result: Result<Data, Error>) -> Void) {
        self.dataTask(with: request) { data, response, error in
            if let response = response as? HTTPURLResponse {
                let statusCode = HTTPStatus(statusCode: response.statusCode)
                
                if statusCode == .success {
                    if let data = data {
                        completion(.success(data))
                    }
                }
                
                guard let error = error else { return }
                
                completion(.failure(error))
            }
        }.resume()
    }
}
