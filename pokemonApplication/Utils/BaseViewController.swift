//
//  BaseViewController.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

protocol BaseViewController {
    associatedtype ViewModel
    
    var viewModel: ViewModel? { get set }
}
