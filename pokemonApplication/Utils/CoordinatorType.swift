//
//  CoordinatorType.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import UIKit

protocol CoordinatorType {
    associatedtype InitialViewController: UIViewController = UIViewController
    
    func makeInitial() -> InitialViewController
}
