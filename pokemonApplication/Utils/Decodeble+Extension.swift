//
//  Decodeble+Extension.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

// MARK: - Decodable

extension Decodable {
    static func decode(from data: Data) throws -> Self {
        return try JSONDecoder().decode(self, from: data)
    }
}

// MARK: - Decodable Array

extension Array where Element: Decodable {
    static func decode(from data: Data) throws -> Array {
        return try JSONDecoder().decode(self, from: data)
    }
}
