//
//  Result+Extension.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

extension Result where Success == Data {
    func decode<T: Decodable>(completion: @escaping (_ result: Result<T, Error>) -> Void) {
        switch self {
        case .success(let data):
            do {
                let decoded = try T.decode(from: data)
                completion(.success(decoded))
            }
            catch {
                completion(.failure(HTTPError.jsonParsingError))
            }
        case .failure(let error):
            completion(.failure(error))
        }
    }
}
