//
//  URLRequest+Extension.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

extension URLRequest {
    mutating func appendHeaders(_ headers: [HTTPHeader]) {
        headers.forEach { self.addValue($0.value, forHTTPHeaderField: $0.key) }
    }
}
