//
//  URL+Extension.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import Foundation

// MARK: - Query

extension URL {
    mutating func appendQueryParameter(key: String, value: String?) {
        guard var components = URLComponents(url: self, resolvingAgainstBaseURL: false) else { return }
        
        var queryItems = components.queryItems ?? [URLQueryItem]()
        
        let matchedItems = queryItems.enumerated().filter { $0.element.name == key }
        
        if let value = value {
            if matchedItems.count > 1 {
                matchedItems.forEach { queryItems.remove(at: $0.offset) }
                queryItems.append(.init(name: key, value: value))
            } else {
                matchedItems.reversed().forEach { queryItems.remove(at: $0.offset) }
            }
        }
        
        if let matchedItem = matchedItems.first, let value = value {
            queryItems[matchedItem.offset] = URLQueryItem(name: key, value: value)
        }
        
        components.queryItems = queryItems
        guard let newUrl = components.url else { return }
        self = newUrl
    }
    
    mutating func removeQueryParameter(key: String) {
        guard var components = URLComponents(url: self, resolvingAgainstBaseURL: false),
              var queryItems = components.queryItems else { return }
        
        let matchedItem = queryItems.enumerated().filter { $0.element.name == key }
        matchedItem.forEach { queryItems.remove(at: $0.offset) }
        
        components.queryItems = queryItems
        guard let newUrl = components.url else { return }
        self = newUrl
    }
    
    mutating func appendQuery(parameters: [String: String]) {
        parameters.forEach {
            self.appendQueryParameter(key: $0.key, value: $0.value)
        }
    }
    
    mutating func removeQuery(parameters: [String: String]) {
        parameters.forEach {
            self.removeQueryParameter(key: $0.key)
        }
    }
}
