//
//  UIWindow+Extension.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 04.10.2021.
//

import UIKit

protocol SingleNavigationControllerType: AnyObject {
    func put(_ item: UIViewController)
}

// MARK: - SingleNavigationControllerType

extension UIWindow: SingleNavigationControllerType {
    func put(_ item: UIViewController) {
        set(rootViewController: item)
    }
}

private extension UIWindow {
    func set(rootViewController newRootViewController: UIViewController) {
        let previousViewConroller = rootViewController
        
        rootViewController = newRootViewController
        
        if let previousViewConroller = previousViewConroller {
            previousViewConroller.dismiss(animated: false) {
                previousViewConroller.view.removeFromSuperview()
            }
        }
    }
}
