//
//  UIViewController+Extension.swift
//  PokemonApplication
//
//  Created by Ivan Morozov on 03.10.2021.
//

import UIKit

extension UIViewController {
    static func loadFromStoryboard() -> Self {
        let storyboard = UIStoryboard(
            name: String(String(describing: self).dropLast("ViewController".count)),
            bundle: Bundle(for: Self.self)
        )
        
        guard let vc = storyboard.instantiateInitialViewController() as? Self else {
            fatalError("Storyboard for \(Self.self) is invalid")
        }
        
        return vc
    }
}
